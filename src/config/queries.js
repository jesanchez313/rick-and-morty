import { gql } from "apollo-boost";

export const GET_CHARACTERS = gql`
  {
    characters {
      results {
        id
        name
        image
        gender
        species
        status
      }
    }
  }
`;

export const GET_EPISODES = gql`
  {
    characters {
      results {
        id
        name
        image
        gender
        species
        status
      }
    }
  }
`;
