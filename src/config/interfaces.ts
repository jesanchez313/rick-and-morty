export interface IAction {
  type: string;
  payload?: any;
}

export interface IHeader {
  title: string;
  description: string;
}

export interface ICharacter {
  id: number;
  name: string;
  image: string;
  gender: string;
  species: string;
  status: string;
  showInfo?: boolean;
}
