import React from "react";
import Header from "components/Header";
import Characters from "components/Characters";

const Home: React.FC = () => {
  return (
    <div className="container">
      <div
        className="c-home d-flex justify-content-center align-items-center"
        data-testid="HomeComponent"
      >
        <div>
          <Header
            title="Rick and Morty"
            description="Rick y Morty es una serie de televisión estadounidense de animación
          para adultos creada por Justin Roiland y Dan Harmon para Adult Swim.
          La serie sigue las desventuras de un científico, Rick, y su fácilmente
          influenciable nieto, Morty, quienes pasan el tiempo entre la vida
          doméstica y los viajes espaciales, temporales e intergalácticos.
          Roiland es el encargado de darle voz a los dos personajes principales,
          la serie también incluye las voces de Chris Parnell, Spencer Grammer,
          y Sarah Chalke."
          ></Header>
          <Characters></Characters>
        </div>
      </div>
    </div>
  );
};

export default Home;
