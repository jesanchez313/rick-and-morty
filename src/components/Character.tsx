import React from "react";
import { ICharacter } from "config/interfaces";

export default function Character({
  name,
  image,
  gender,
  status,
  species,
  showInfo
}: ICharacter) {
  return (
    <div className="card mb-3">
      <h3 className="card-header" id="character-name">
        {name}
      </h3>
      <img src={image} alt={name} />
      {showInfo && (
        <ul className="list-group list-group-flush">
          <li className="list-group-item d-flex justify-content-between">
            <span>Species: </span>
            <span>{species}</span>
          </li>
          <li className="list-group-item d-flex justify-content-between">
            <span>Gender: </span>
            <span>{gender}</span>
          </li>
          <li className="list-group-item d-flex justify-content-between">
            <span>Status: </span>
            <span>{status}</span>
          </li>
        </ul>
      )}
    </div>
  );
}
