import React from "react";
import { IHeader } from "config/interfaces";

const Header: React.FC<IHeader> = ({ title, description }) => {
  return (
    <div className="mt-5">
      <div className="jumbotron">
        <h1 className="display-4">{title}</h1>
        <p className="lead">{description}</p>
        <div>
          <button type="button" className="btn btn-primary">
            Episodes list
          </button>
        </div>
      </div>
    </div>
  );
};

export default Header;
