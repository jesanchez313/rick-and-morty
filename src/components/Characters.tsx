import React from "react";

import { useQuery } from "@apollo/react-hooks";
import { GET_CHARACTERS } from "config/queries";
import { ICharacter } from "config/interfaces";
import Character from "components/Character";

const Characters = () => {
  const { loading, error, data } = useQuery(GET_CHARACTERS);

  if (loading) return <h1>Loading...</h1>;
  if (error) return <h2>{`Error! ${error.message}`}</h2>;

  return (
    <div className="mt-5">
      <div className="row">
        {data.characters.results.map((character: ICharacter) => (
          <div className="col-4" key={character.id}>
            <Character {...character}></Character>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Characters;
