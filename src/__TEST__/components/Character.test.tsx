import React from "react";
import { render } from "@testing-library/react";
import Character from "components/Character";

describe("Test all character component", () => {
  it("should render basic component", () => {
    const name = "Pepito";
    const mock = {
      id: 1,
      name,
      image: "test.img",
      gender: "male",
      species: "human",
      status: "single",
      showInfo: false
    };

    const { container } = render(<Character {...mock}></Character>);

    const elementName = container.querySelector("#character-name").textContent;
    expect(elementName).toEqual(name);
  });

  it("should render basic component", () => {
    const name = "Pepito";
    const mock = {
      id: 1,
      name,
      image: "test.img",
      gender: "male",
      species: "human",
      status: "single",
      showInfo: true
    };

    const { container } = render(<Character {...mock}></Character>);

    const elementName = container.querySelector("#character-name").textContent;
    expect(elementName).toEqual(name);
  });
});
